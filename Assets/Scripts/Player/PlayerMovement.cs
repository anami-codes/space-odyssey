﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour{

    public RectTransform aim;
    public ParticleSystem cannon_1;
    public ParticleSystem cannon_2;

    public float speed = 1f;
    public float shootTime = 1f;

    public ShipArea[] ship = new ShipArea[3];
    public int damage = 1;

    private Ray ray;
    private RaycastHit hit;
    private Vector3 mousePos;

    private Vector3 rot = Vector3.zero;
    private float h;
    private float v;

    private float shootTimer;
    private float stopShootTimer;
    private bool shooting;

    void Start() {
    }

    void Update() {
        h = Input.GetAxisRaw("Horizontal");
        rot.y = (h * speed * Time.deltaTime);
        transform.Rotate(rot);

        mousePos = Input.mousePosition;
        aim.position = mousePos;

        if (Input.GetMouseButton(0) && shootTimer <= Time.time && !shooting)
            Shoot();

        if (stopShootTimer <= Time.time && shooting)
            StopShoot();
    }

    void Shoot () {
        Vector3 worldPoint = mousePos;
        worldPoint.z = 100;
        worldPoint = Camera.main.ScreenToWorldPoint(worldPoint);

        cannon_1.transform.LookAt(worldPoint);
        cannon_2.transform.LookAt(worldPoint);

        cannon_1.Play();
        cannon_2.Play();

        Vector3 dir = worldPoint - transform.position;
        ray = new Ray(transform.position, dir);
        if (Physics.Raycast(ray, out hit)) Attack(hit.transform);

        shootTimer = Time.time + shootTime;
        stopShootTimer = Time.time + 0.25f;
        shooting = true;
    }

    void StopShoot () {
        cannon_1.Stop();
        cannon_2.Stop();

        shooting = false;
    }

    void Attack (Transform target) {
        if (target.CompareTag("Enemy")) {
            target.GetComponent<EnemyBehaviour>().ReceiveDamage(damage);
        }
    }
}
