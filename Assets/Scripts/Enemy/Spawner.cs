﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemy;

    public Transform min;
    public Transform max;

    public float spawnTime = 1f;
    public float radius = 100f;

    private List<EnemyBehaviour> enemies;
    private float spawnTimer;

    void Start() {
        
    }

    void Update() {
        if (spawnTimer <= Time.time) Spawn();
    }

    void Spawn () {
        float angle = Random.Range(0, 360);
        float x = radius * Mathf.Cos(angle);
        float y = Random.Range(-31, 30);
        float z = radius * Mathf.Sin(angle);
        Vector3 pos_ = new Vector3(x, y, z);

        GameObject enemy_ = Instantiate(enemy, pos_, transform.rotation);

        x = Random.Range(min.position.x, max.position.x);
        z = Random.Range(min.position.z, max.position.z);
        Vector3 dir_ = new Vector3(x, 0, z);
        enemy_.GetComponent<EnemyBehaviour>().Activate(dir_, angle);

        spawnTimer = Time.time + spawnTime;
    }
}
