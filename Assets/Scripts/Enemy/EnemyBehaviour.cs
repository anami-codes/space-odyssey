﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public int life = 3;
    public float speed = 1f;

    private float spawnAngle;

    public void Activate(Vector3 target, float angle_) {
        transform.LookAt(target);
        spawnAngle = angle_;
    }

    void Update() {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        Sonar.instance.CheckGlobalPos(spawnAngle);
    }

    public void ReceiveDamage (int dmg) {
        life -= dmg;
        if (life <= 0) Death();
    }

    void Death () {
        Destroy(gameObject, 0.5f);
    }
}
