﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonar : MonoBehaviour {

    static public Sonar instance;

    public Transform player;

    public GameObject leftArrow;
    public GameObject rightArrow;

    void Start() {
        if (instance == null) instance = this;
    }

    void Update() {
        
    }

    public void CheckGlobalPos (float enemyAngle) {
        float playerAngle = player.eulerAngles.y;
        if (playerAngle > 360 || playerAngle < -360)
            playerAngle /= 360;

        float minAngle = playerAngle - 30;
        float maxAngle = playerAngle + 30;

        if (minAngle < 0) minAngle += 360;
        if (maxAngle > 360) maxAngle -= 360;

        if (enemyAngle < minAngle)
            leftArrow.SetActive(true);
        else if (enemyAngle > maxAngle)
            rightArrow.SetActive(true);
    }
}
